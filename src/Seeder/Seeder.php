<?php

namespace Newebtime\StreamsExtra\Seeder;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Anomaly\Streams\Platform\Model\EloquentModel;
use Illuminate\Database\Seeder as BaseSeeder;

/**
 * Class Seeder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Seeder extends BaseSeeder
{
    /**
     * @var EntryRepository|string
     */
    protected $repository;

    /**
     * Define if the seeder should truncate tables
     *
     * @var boolean
     */
    protected $truncate = true;

    /**
     * Extra tables to truncate
     *
     * @var array
     */
    protected $truncatable = [];

    public function __construct()
    {
        if (is_string($this->repository)) {
            $this->repository = app($this->repository);
        }
    }

    /**
     * Seed many entry from an array
     * 
     * @param array   $data
     * @param integer $parent
     */
    public function seedMany(array $data, $parent = null)
    {
        foreach ($data as $item) {
            if (isset($item['children'])) {
                $children = $item['children'];

                unset($item['children']);
            } else {
                $children = null;
            }

            if (isset($parent)) {
                $item['parent_id'] = $parent;
            }

            $model = $this->seedOne($item);

            if (isset($children)) {
                $this->seedMany($children, $model->getId());
            }
        }
    }

    /**
     * Seed on entry
     *
     * @param array $data
     *
     * @return EloquentModel
     */
    public function seedOne(array $data)
    {
        $attr  = array_except($data, 'relations');
        $model = $this->repository->create($this->extraSeed($attr) + $attr);

        $this->afterSeed($model, $data);

        return $model;
    }

    /**
     * Seed extra data, used for static data or programmably generated one
     *
     * @param array $data
     *
     * @return array
     */
    protected function extraSeed(array $data): array
    {
        return [];
    }

    /**
     * After seeding (e.g. attach relations, etc)
     *
     * @param $model
     * @param array $data
     */
    protected function afterSeed($model, array $data)
    {
        return;
    }

    protected function truncate()
    {
        foreach ($this->truncatable as $truncate) {
            \DB::table($truncate)->truncate();
        }
    }
}
