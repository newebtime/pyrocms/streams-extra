<?php

namespace Newebtime\StreamsExtra\Seeder;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * DataSeeder class.
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class DataSeeder extends Seeder
{
    /**
     * Path where is stored the file, relative to database_path()
     *
     * @var string
     */
    protected $filePath = 'data';

    /**
     * The name of the file to import, if null $fileType is required
     *
     * @var null|string
     */
    protected $fileName = null;

    /**
     * Force the import file type [csv, array], if null $fileName is required
     *
     * @var null|string
     */
    protected $fileType = null;

    /**
     * The type of seed [query, model], query is faster but doesn't trigger event/observer
     *
     * @var string
     */
    protected $seedType = 'query';

    /**
     * An array of pivot and type to import (e.g. ['types' => 'array'])
     *
     * @var array
     */
    protected $importPivots = null;

    /**
     * The columns matching (csv)
     *
     * @var array
     */
    protected $matching = [];

    /**
     * @var array
     */
    protected $options = [
        'delimiter' => ',',
    ];

    public function __construct()
    {
        parent::__construct();

        if (!$this->fileName && !$this->fileType) {
            throw new \Exception('$fileName or $fileType need to be set.');
        }

        if (!$this->fileName) {
            $basename = snake_case(str_singular(substr(class_basename($this->repository->getModel()), 0, -5)));

            $this->fileName = snake_case($basename) . ($this->fileType === 'csv' ? '.csv' : '.php');
        }

        if (!$this->fileType) {
            $this->fileType = (strstr($this->fileName, '.csv') === '.csv') ? 'csv' : 'array';
        }
    }

    protected function seedData()
    {
        if (!$this->truncate) {
            $this->repository->newQuery()->truncate();

            $this->truncate();
        }

        $this->{'import' . ucfirst($this->fileType)}();
        $this->importPivots();
    }

    protected function importCsv()
    {
        $i      = 0;
        $items  = [];
        $handle = fopen(database_path($this->filePath . '/' . $this->fileName), "r");

        // Note: We are insering the data 100 by 100
        while (($data = fgetcsv($handle, 0, $this->options['delimiter'])) !== false) {
            $items[] = array_map(function ($value) use ($data) {
                if ($value === 'now') {
                    return Carbon::now();
                }

                return $data[$value];
            }, $this->matching);

            $i++;

            if ($i == 100) {
                if ($this->seedType === 'query') {
                    $this->repository->newQuery()->insert($items);
                } else {
                    $this->seedMany($items);
                }

                $items = [];
                $i     = 0;
            }
        }

        if ($this->seedType === 'query') {
            $this->repository->newQuery()->insert($items);
        } else {
            $this->seedMany($items);
        }
    }

    protected function importArray()
    {
        $items = require database_path($this->filePath . '/' . $this->fileName);

        // Note: Sqlite variable limit is 999 by default
        $max = floor(999 / count($items));

        foreach (array_chunk($items, $max, true) as $insert) {
            $this->repository->newQuery()->insert($insert);
        }
    }

    protected function importPivots()
    {
        if (!$this->importPivots) {
            return;
        }

        // Note: Only "array" is supported for now
        foreach ($this->importPivots as $pivot => $type) {
            $items = require database_path('data/' . $this->fileName . '_' . $pivot . '.php');

            $pivot = camel_case($pivot);
            $table = $this->repository->getModel()->$pivot()->getTable();

            // Note: Sqlite variable limit is 999 by default
            $max = floor(999 / count($items[1]));

            foreach (array_chunk($items, $max, true) as $insert) {
                DB::table($table)->insert($insert);
            }
        }
    }
}
