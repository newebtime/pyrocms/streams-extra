<?php

namespace Newebtime\StreamsExtra\Seeder;

use Anomaly\NavigationModule\Link\LinkRepository;
use Anomaly\NavigationModule\Menu\Contract\MenuInterface;
use Anomaly\NavigationModule\Menu\MenuRepository;
use Anomaly\Streams\Platform\Addon\Extension\Extension;
use Anomaly\Streams\Platform\Addon\Extension\ExtensionCollection;
use Anomaly\Streams\Platform\Entry\EntryRepository;
use Anomaly\Streams\Platform\Model\EloquentCollection;
use Anomaly\Streams\Platform\Model\EloquentModel;

/**
 * Class NavigationLinksSeeder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
abstract class NavigationLinksSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $truncatable = [
        'navigation_links',
        'navigation_links_allowed_roles',
    ];

    /**
     * @var LinkRepository
     */
    protected $repository;

    /**
     * @var EloquentCollection|MenuInterface[]
     */
    protected $menus;

    /**
     * @var EntryRepository[]
     */
    protected $links;

    /**
     * NavigationSeeder constructor.
     *
     * @param LinkRepository      $repository
     * @param MenuRepository      $menus
     * @param ExtensionCollection $extensions
     */
    public function __construct(LinkRepository $repository, MenuRepository $menus, ExtensionCollection $extensions)
    {
        $this->repository = $repository;
        $this->menus      = $menus->all()->keyBy('slug');
        $this->links      = $extensions
            ->search('anomaly.module.navigation::link_type.*')
            ->enabled()
            ->keyBy('namespace')
            ->map(
                function (Extension $extension) {
                    $modelName = substr(get_class($extension), 0, -9) . 'Model';
                    /** @var EloquentModel $model */
                    $model     = new $modelName();

                    array_push($this->truncatable, $model->getTable());

                    if ($model->isTranslatable()) {
                        array_push($this->truncatable, $model->getTranslationTableName());
                    }

                    return (new EntryRepository())->setModel($model);
                }
            );
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->truncate) {
            $this->truncate();
        }

        foreach ($this->dataLinks() as $links) {
            $this->seedMany($links);
        }
    }

    abstract protected function dataLinks(): array;

    /**
     * @inheritdoc
     */
    protected function extraSeed(array $data): array
    {
        return [
            'entry'  => $this->seedEntry($data['entry'], $data['type']),
            'menu'   => $this->menus->get($data['menu']),
            'target' => '_self',
        ];
    }

    /**
     * @param array  $data
     * @param string $type
     *
     * @return EloquentModel
     */
    protected function seedEntry(array $data, $type)
    {
        return $this->links->get($type)->create($data);
    }
}
